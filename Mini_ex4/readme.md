Allow access to webcam for it to work
https://glcdn.githack.com/NicolaiRieder/ap/raw/master/Mini_ex4/p5/empty-example/index.html

[!Screenshot](messenger.PNG)

I decided to handle the 'CAPTURE ALL' theme quite literally, using a lot of the syntaxes I have used the previous weeks and createCapture(VIDEO); to capture the user's webcam. 
The approach I took ended up being a 'simulation', to a certain degree, of how an install of Facebook's "Messenger" app is handled for all its functions to work. One thing that has become quite obvious the last couple of years is that not only are these permissions used for the app's functions to work, they are also used by Facebook in a way that the average user doesn't necessarily consider when agreeing to the terms of service/usage. Facebook uses data from the microphone, camera and location to show the most relevant ads for the user, ie. to make Facebook more money.

I had some problems with the mouseIsPressed function early on in the coding process, which led to me realizing that the 'button' function was probably the better choice for my project. I also used a short array to defined what text should appear on the button.
It ended up quite nice, however I did originally want a throbber to be next to the "Listening for advertising keywords" text, but it seemed to stay still. I assume the reason for this is the capture(VIDEO) that somehow is 'preferred' and therefore decides how the program acts. Not sure if it is possible to load both a video via webcam (or a video at all), while 'creating a throbber'. While writing this I realised that I have simply been stupid and put the drawThrobber syntax in the wrong function...

Ok, I'm back and now the throbber seems to work as it's supposed to so that's good. Just shows how a bit of contemplation can lead you to the right result. The host link might not show (since it seems to never update after something changes in the index.html file), but it works on my side.




