let messenger;
let boxX = 240;
let boxY = 520;
let by = 60;
let bx = 200;
var overbox =false;
var called = false;
var numClicks = 0;
var buttontext = ["Install app", "Allow", ":D"];
var iterator=0
let capture;


function setup() {
  createCanvas(1920, 1080);
  messenger = loadImage('assets/mes.png');
  background(0, 60, 255);
  installapp();
  capture = createCapture(VIDEO);
  capture.size(320, 240);
  capture.hide();
  frameRate(35)



}

function draw() {
  translate(0, -25)
  push()
  fill(0, 60, 255)
  noStroke()
  rect(1150,635,30,30)
  pop()
  image(messenger, 185, 120);
  button = createButton(buttontext[iterator]);
  button.position(boxX, boxY);
  button.size(bx, by)
  button.mousePressed(allow);

  if (numClicks>=4){
    translate(1000, 350)
    image(capture, 0, 0, 320, 240);
    drawThrobber(60)
  }



}

function installapp() {

  push()
  fill(255);
  fill(0);
  textSize(32);
  text('Install App', 265, 560)
  pop()

}
  function allow() {
    numClicks++;
    if (numClicks==1){
    iterator++;
    fill(0)
    textSize(32)
    fill(255)
    text('Allow access to phonebook?', 135, 490)
  } else if (numClicks==2){
    push();
    textSize(30);
    fill(0, 255, 100)
    text('\u2714', 850, 100)
    fill(255)
    textSize(20)
    text('      Access to all contacts\' phone numbers and names complete', 850, 100)
    pop();
    textSize(32);
    fill(0, 60, 255);
    noStroke()
    rect(100,450, 500, 60)
    fill(0);
    translate(30, 0)
    fill(255);
    text('Allow access to images and videos?', 68, 490);
  } else if (numClicks==3) {
    textSize(30);
    fill(0, 255, 100)
    text('\u2714', 850, 150)
    fill(255)
    textSize(20)
    text('      Access to all images and videos in users library complete', 850, 150)
    pop();
    fill(0, 60, 255);
    noStroke()
    rect(50, 450, 700, 60)
    fill(0);
    textSize(32);
    translate(30, 0)
    fill(255);
    text('Allow access to camera, microphone and location?', 0, 490);
  } else if (numClicks==4) {
    iterator++;
    textSize(30);
    fill(0, 255, 100)
    text('\u2714', 850, 200)
    fill(255)
    textSize(20)
    text('      Access to camera and microphone at all times complete', 850, 200)
    text('Surveillance initiated:', 1065, 330)
    text('*\\Listening for advertising keywords/*', 995, 620)
    pop();
    fill(0, 60, 255);
    noStroke()
    rect(30, 450, 800, 60)
    textSize(32);
    translate(30, 0)
    fill(255);
    text('Everything is good to go; enjoy Messenger!', 20, 490);

  }
}
function drawThrobber(num) {
  push();
  translate(165, 300); //move things to the center
  // 360/num >> degree of each ellipse' move ;frameCount%num >> get the remainder that indicates the movement of the ellipse
  let cir = 360/num*(frameCount%num);  //to know which one among 9 possible positions.
  rotate(radians(cir));
  noStroke();
  fill(0);
  ellipse(10,0,10,10);  //the moving dot(s), the x is the distance from the center
  pop();
}
