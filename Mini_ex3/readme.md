Please do give it a little while to load - it took some time om my side:
https://glcdn.githack.com/NicolaiRieder/ap/raw/master/Mini_ex3/p5/empty-example/index.html

One thing that has literally been pissing me off is that none of the host sites seem to be able to load the index.html as it looks on my live server
Statically loaded it correctly besides not being able to load the video:
https://cdn.staticaly.com/gl/NicolaiRieder/ap/raw/master/Mini_ex3/p5/empty-example/index.html

The idea is that the video that appears is also centered and not off-centered as I have seen a couple of times now via the githack link. Another problem has been that githack seems to load the video under the canvas so one could scroll down and see it under the yellow canvas. This is not supposed to happen and does not happen in my live server, the code seems to be correct which leads me to believe that it is indeed a hosting issue.

This week I have spent a lot of time trying to get the video to work and while doing that I encountered a lot of problems regarding how the code should be written to work how it was intended. One of the problems I had included the function added to play and pause the video, as I wanted the video to stop while the mouse button was being held down and pause when said button was released, luckily I found a way for it work. I also ended up finding a way to load both the video, the image and make the "stackable" which I had a great deal of trouble with.

My throbber itself was not intended as any critical comment on anything. The spinning motion of the throbbers simply reminded me of the old Batman intro and the spinning batman logo that was used as a transition in the cartoon for many years.

To make the throbber I used some of Winnie's code for the circle in the middle and adjusted it to my needs while also adding an image of the batman logo which I then set to rotate the opposite direction of the previously mentioned circle. The syntaxes used and some of their definitions can be read inside the source code, i.e. the sketch.js file found in /p5/empty-example/sketch.js










![Screenshot](bm.PNG) ![Screenshot](bm2.png)


