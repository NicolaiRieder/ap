[Mini_ex9 code can be found here](https://gitlab.com/NicolaiRieder/ap/tree/master/Mini_ex9)


[A live version of the program in action can be seen here](https://cdn.staticaly.com/gl/NicolaiRieder/ap/raw/master/Mini_ex9/index.html)

![Screenshot](miniex9.PNG)


The difficulties in drawing flow charts come down to how accurate and detailed the flow charts have to be. Sometimes it is hard to decide whether or not a certain step or part of a process is necessary to include in the flowchart as that process might not seem vital to one individual however another individual might find it a key part in how a computer gets to the next part in the process.

It is hard to limit the amount of information put in the flow chart as all information could be argued valuable in regards to how the computer works through its tasks.

![Screenshot](flowchart_throbber.PNG)


![Screenshot](flowchart_game.PNG)

While the game (the bottom image) is a fairly complicated thing to code, the top image shows quite the opposite. It's a flowchart showing quite the meta idea of how a throbber would slowly, and steadily, progress while showing different sentences related to writing an exam. Of course it wouldn't actually write anything, it would merely appear as if it was making progress but then crash at the end as the "assignement" wouldn't be good enough. 

The code for this would be rather simple and with our current knowledge of coding we don't think of it as a hard and challenging task.

The game however, has quite a lot of aspects that could cause us trouble were we to try and actually create it. Not only could we end up having problems with the objects not colliding properly, we could also risk having problems with the upgrade screen in-between levels (and thereby the upgrades themselves). We find this option the most challenging but also the most interesting of the two.


The are some similarities between the flow chart from mini_ex9 and the throbber one that are quite obvious. Both go load JSON files, show something on-screen and then proceed to repeat this process for eternity. The "hackman" game however has little to no similarities in how it works, which makes the flow chart differ quite a bit from the two others.


The notion of algorithms is not that far off the way society itself, and more specifically jobs, function. There are a lot of rules that are set from the get-go that lead humans/workers to make certain decisions and produce different outcomes. Different sectors can have their own set of "algorithmic" processes the go through. A gardener might know when to cut of a branch on a tree for it to flourish, a wall-street broker might know that if certain signs appear it is time to either invest or sell and so it continues. All these different aspects of jobs and other parts of life can lead us to make certain decisions, just like the computer does when it realises that A=true but B=false. These specific parameters lead the computer to further decision-making/processes.  