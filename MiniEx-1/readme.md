![Screenshot](mcd.png)
https://cdn.staticaly.com/gl/NicolaiRieder/ap/raw/master/MiniEx-1/p5/empty-example/index.html

**Coding Process**

When setting out to trying to create something independently in Atom I encountered several problems and/or struggles. 
It's not the first time I have been programming as I have previously worked with *HTML*, however this language (i.e. *Javascript*) is new to
me.
Starting off I didn't have an idea as to what my code should produce, so I tried some of the different syntaxes, such as several 3D-shapes (ellipsoid(), box() and so on) and after using an hour on an ellipsoid, with (2D) squares around it, spinning as if there was no tomorrow I decided that it simply wasn't good enough, so I scratched that.

Before doing anything else though I set up the canvas with createCanvas(windowWidth, windowHeight, WEBGL); so it would resize itself to different window sizes upon a refresh. This was followed by me writing a random hex code for background(); I ended up quite liking the produced colour.

Now, the following choice might have been influenced by me being quite hungry at the time, but I decided to imitate the McDonald fries box: [Click here to see inspiration](https://akns-images.eonline.com/eol_images/Entire_Site/2018619/rs_1024x1024-180719122106-1024-mcdonalds-fries.jpg?fit=inside|900:auto&output-quality=90)

I started out making the "fries", in my case I used the box() syntax to define the height, width and depth of the boxes, as an example: box(50, 300, 50). After being satisfied with the size of the box I decided to duplicate the code to have more boxes that I then moved around with translate().
It was here that I realized that translate() not only moves whatever shape's code it is next to, but also every bit of code that produces shapes that followed. As I couldn't seem to find any way to make sure that translate() only affected a certain box, I had to individually edit the following translate() usages so the boxes ended up next to each other the way that I wanted. 

After getting all the boxes positioned I had to figure out how to colour them all. After looking a bit at the different syntaxes I noticed that fill() seemed to be the one I had to use. I then googled the hex code for McDonald's' yellow logo as I wanted the fries to be the same colour. Now I inserted the fill('#FFC72C') right before all the box() syntaxes assuming it would colour/fill whatever followed, and it was a success. 

Then the time came for the box that should imitate the red carton/cardboard box that McDonald's fries are served in. I once again used box() and modified the size until it covered the fries (box(260, 320, 150);), however, I also had to use translate() for it to cover them completely. From my previous google search of McDonald's colours I also had the hex code for the box, so I used that to fill() the box as well. All code regarding the outer box I had placed above the code for the individual boxes resembling fries. 

I wanted all these individual shapes to rotate the same way at the same time so it would appear as one object, this I did by placing the code for the rotation before all the objects in the code. The rotation was also implemented to show off the three dimensional aspects of the shapes. This I did with the following code:  rotateX(frameCount * 0.01); rotateY(frameCount * 0.02); rotateZ(frameCount * 0.01);. I added the Z rotation and adjusted the y rotation so the rotation wouldn't just be one directional all the time. 

I really wanted to include the yellow M which is one of the logos that McDonald's uses. I found a font called "McLawsuit" (I think it's a good thing that my code is not used for any commercial uses) that included the M shape. I had to look into how I could insert a font which turned out quite easy. I had to function preload() the font (from the asset folder, the font is defined as m) and then in function (setup) define how the text should be aligned and how big it should be. Then in function draw() I simply had to callback the previously loaded font by using text('m', 0, 0) (which I did twice to get an M on each side of the red box), then followed by translate() to place the m's on each of the sides. Because of the fill() previously used to fill the "fries" also colouring the m's I didn't have to define a colour once again.

The whole process was defined and restricted by my lack of knowledge regarding Javascript and coding in general, as I couldn't do all the things I wanted to do for example make the red box the same shape as what it was supposed to imitate. 


Writing text, such as this readme, and coding have both similarities and major differences. I do not have a problem expressing or articulating my ideas via text as it is a language (both English and Danish) that I believe to be fluent in to some degree. Coding however, is a language that I am not yet fluent in: I might be able to write a couple of basic syntaxes and make them work, just as I would be able to use a couple of sentences while learning a completely new human language such as hungarian or russian, but one thing that I at this point can't do is writing something more coherent than that, something grander and more complex. This is something I would like to be better at in the future so I via different coding languages can express the ideas I have accurately. 

This means that coding to me is a way to express oneself, a way to show off ideas: both visually and via interaction. It is a different way to communicate with others while still being similar to our daily interaction by use of text or speech. 