var url = "https://api.nasa.gov/planetary/earth/imagery/?"
var dim = "0.065"
var date = "2016-09-2" //2014-2017
var apikey = "Nc5HaQgcArEWjiSApf64F66AEN7xPEZf4WoPBebc";
//Nc5HaQgcArEWjiSApf64F66AEN7xPEZf4WoPBebc
//8t79CdOvA7wc6WJLwNoLW8dwRp33CDgT3eBzeCqs
var p = 0
var o = 0
var x = 0
var rand
var g_lon
var h_lat
var img;
var getImg;
var cv; //Center the image
var request;


function setup() {
	cv = createCanvas(windowWidth,windowHeight);
	setInterval(fetchImage, 2000);
	centerCanvas();
	background(random(150,200),random(150,200),random(150,200));
	frameRate(2);

}

function centerCanvas() {
	let x = (windowWidth - width) / 2;
  let y = (windowHeight - height) / 2;
  cv.position(x, y);

}

function gotData(data) {   //a callback needs an argument
	getImg = data.url  // this is the thumbnail

}

function draw() {
	try {	//takes time to load the external image, that's why you see errors in the console.log
		images();
	}catch(error) {
  	console.error(error)


	}
}

function images() {

	loadImage(getImg, function(img) {
	push();
	translate(0, 0);

	image(img,random(0,windowWidth),random(0,windowHeight), 100, 100);
	pop();
})
}



function fetchImage() {
	rand();
	request = url + "lon=" + g_lon + "&lat=" + h_lat + "&dim=" + dim + "&date=" + date + "&api_key=" + apikey
	console.log(request);
	loadJSON(request, gotData);
}
	function rand() {
	   g_lon = Math.floor((Math.random() * 359))
	   h_lat = Math.floor((Math.random() * 89))

}
